from urllib import request
import re

Name = r'<a href=".*">(.*)</a></h3>'
Location = r'<span class="event-location">(.*)</span>'
Time = r'<time datetime="(.*)T.*">.*</time>'

def main():
    URL = 'https://www.python.org/events/python-events/'    
    data = delInf(URL)
    
def delInf(URL):
    strInf = request.urlopen(URL).read().decode('utf-8')
    name = re.findall(Name, strInf)
    location = re.findall(Location, strInf)
    time = re.findall(Time, strInf)
    for index in range(0, len(name)):
        print('会议名称：'+ name[index])
        print('会议地点：'+ location[index])
        print('会议时间：'+ time[index] + '\n')
        
if __name__ == '__main__':
    main()