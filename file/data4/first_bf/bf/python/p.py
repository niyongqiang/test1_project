#枪类
class Gun():
    def __init__(self,model):
        self.model = model
        self.bullet_count = 0
        #添加子弹
    def add_bullet(self,count):
        self.bullet_count += count
        #射击
    def shoot(self):
        if self.bullet_count <= 0:
            print('[%s]没子弹了' % self.model)
            return#没子弹就不继续执行了
        self.bullet_count -= 1
        print('[%s]发射子弹‘突突突’，子弹剩余[%d]' % (self.model,self.bullet_count))
#士兵类
class Soldier(object):
    def __init__(self,name):
        self.name = name
        self.gun = None
            
#创建枪对象
ak47 = Gun('AK47')
ak47.add_bullet(50)
ak47.shoot()
#创建士兵对象
xusanduo = Soldier('许三多')
xusanduo.gun = ak47
print(xusanduo.gun)

