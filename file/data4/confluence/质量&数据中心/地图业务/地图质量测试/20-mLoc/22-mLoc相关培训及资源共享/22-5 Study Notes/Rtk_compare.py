import sys
import os
import math
import numpy as np
from matplotlib import pyplot as plt

def deal_with_data(data_path):
    data = open(data_path,'r')
    lines_data = data.readlines()
    data.close()
    True_Lines_data = [i for i in lines_data if i[0].isdigit() is False]

    List_data = []
    for m in True_Lines_data:
        mm = m.split()
        List_data.append(mm)
    length_data = len(List_data)
    up_point_line_List = []
    up_point_sign_List = []
    for i in range(length_data-3):
        if List_data[i+1][0][0] == List_data[i][0][0] == "L" and List_data[i+1][0][2] == List_data[i][0][2]:
            up_point_data_line = [List_data[i][0],List_data[i][1],List_data[i][2]]
            up_point_line_List.append(up_point_data_line)
        if List_data[i + 3][0][0] == List_data[i + 2][0][0] == List_data[i + 1][0][0] == List_data[i][0][0] == "P" and \
                List_data[i + 3][0][2] == List_data[i + 2][0][2] == List_data[i + 1][0][2] == List_data[i][0][2]:
            up_point_data_sign = [List_data[i][0], List_data[i][1], List_data[i][2]]
            up_point_sign_List.append(up_point_data_sign)
    return up_point_line_List,up_point_sign_List

def find_min_dis(line_a,line_b):
    for m in line_b:
        dis_line_List = []
        for n in line_a:
            dis_line = math.sqrt((float(m[1]) - float(n[1])) ** 2 + (float(m[2]) - float(n[2])) ** 2)
            # print("dis_line:",dis_line)
            dis_line_List.append(dis_line)
            # print("dis_line_List:",dis_line_List)
        min_dis = min(dis_line_List)
        print("min_distance:", min_dis)
        return min_dis

def draw(line_a,line_b):
    a = np.array(line_a)
    b = np.array(line_b)
    a = a[:, 1:].astype(float)
    b = b[:, 1:].astype(float)
    plt.plot(a[:, 1], a[:, 0], "og")
    plt.plot(b[:, 1], b[:, 0], "or")
    plt.show()

def main():
    result_a = deal_with_data('/home/haoran/桌面/0918_paigan.txt')
    result_b = deal_with_data('/home/haoran/桌面/sum_total_station_point.txt')
    find_min_dis(result_a[0],result_b[0])
    draw(result_a[0],result_b[0])


if __name__ == "__main__":
    main()