import os
import sys


def delete_data(tag):
    folder_list = ["/data1", "/data2", "/data3", "/data4", "/data5",
                   "/data6", "/data7", "/data8", "/data9", "/B51", "/B5"]
    for i in folder_list:
        for root, dirs, files in os.walk(i):
            print(root)
            print(dirs)
            print(files)
            for name in dirs:
                if name == tag:
                    os.rmdir(os.path.join(root, name))
                    break


if __name__ == "__main__":
    tag = sys.argv[1]

    delete_data(tag)
