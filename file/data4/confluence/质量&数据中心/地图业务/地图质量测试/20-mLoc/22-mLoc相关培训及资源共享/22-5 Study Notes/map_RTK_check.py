import sys
import os
import shutil
import math
import json
import numpy as np
from matplotlib import pyplot as plt
from pyproj import Proj
from pyproj import transform
import xlrd
import xlwt
from xlutils.copy import copy

#经纬高转utm
def wgs2utm_proj4(lla):
    ll = Proj(proj='latlong', datum='WGS84')
    utm = Proj(proj='utm', zone=51, datum='WGS84')
    (x, y) = transform(ll, utm, lla[1], lla[0])
    return np.array([x, y, lla[2]])
#utm转经纬高
def utm2wgs_proj4(xyz):
    ll = Proj(proj='latlong', datum='WGS84')
    utm = Proj(proj='utm', zone=51, datum='WGS84')
    (lon, lat) = transform(utm, ll, xyz[0], xyz[1])
    return np.array([lat, lon, xyz[2]])

#杆数据转为json
def pole_data_to_json(id,pts):
  pole_json = {}
  pole_json["m_id"] = id
  pole_json["m_coord"] = {"lat":pts[0][0],"lon":pts[0][1],"alt":pts[0][2]}
  pole_json["m_prob"] = 1.0
  pole_json["original_id"] = id
  pole_json["m_kpoints"] = []
  for i in range(len(pts)):
    pole_json["m_kpoints"].append({"lat":pts[i][0],"lon":pts[i][1],"alt":pts[i][2]})
  return pole_json

#牌数据转为json
def trafficsign_data_to_json(id,pts):
  trafficsign_json = {}
  trafficsign_json["m_id"] = id
  trafficsign_json["m_coord"] = {"lat":pts[0][0],"lon":pts[0][1],"alt":pts[0][2]}
  trafficsign_json["m_prob"] = 1.0
  trafficsign_json["original_id"] = id
  trafficsign_json["m_shape"] = "rectangle"
  trafficsign_json["m_complete_number"] = 4
  trafficsign_json["m_kpoints"] = []
  for i in range(len(pts)):
    trafficsign_json["m_kpoints"].append({"lat":pts[i][0],"lon":pts[i][1],"alt":pts[i][2]})
  return trafficsign_json


# 将真值数据转化为json数据
def txt_to_json(txt_data):
    # load data from txt
    poles = {}
    trafficsigns = {}
    ids = txt_data[:, 0]
    pts = txt_data[:, 1:].astype(float)
    for i in range(len(ids)):
        pt_id = ids[i]
        pt = pts[i, :]
        pt[0], pt[1] = pt[1], pt[0]
        pt = utm2wgs_proj4(pt)
        if (len(pt_id) < 3):
            continue
        pt_type = pt_id[0]
        item_id = pt_id[1:][:-1]
        order_id = pt_id[-1]
        if (pt_type == "L"):
            if item_id not in poles:
                poles[item_id] = [list(pt)]
            else:
                poles[item_id].append(list(pt))
        elif (pt_type == "p"):
            if item_id not in trafficsigns:
                trafficsigns[item_id] = [list(pt)]
            else:
                trafficsigns[item_id].append(list(pt))
        else:
            print("ERROR pt type:%s" % pt_id)

    # change traffcsign pt order
    for id in trafficsigns:
        t = trafficsigns[id]
        if (len(t) != 4):
            print("ERROR only support trafficsign point is 4, but is %d", len(t))
        t[1], t[2], t[3] = t[3], t[1], t[2]

    # set to json
    json_data = {"m_pole_list": [],
                 "m_traffic_sign_list": [],
                 "m_dash_endpoint_list": [],
                 "m_section_list": [],
                 "m_next_curve_map": {},
                 "m_next_lane_map": {},
                 "m_next_section_map": {}
                 }
    for id, pts in trafficsigns.items():
        json_data["m_traffic_sign_list"].append(trafficsign_data_to_json(id, pts))
    for id, pts in poles.items():
        json_data["m_pole_list"].append(pole_data_to_json(id, pts))
    return json_data


# utm坐标系的json转化为投影坐标系
def json_pts_to_np(json_pts):
    pts = np.zeros((len(json_pts), 3))
    for i in range(len(json_pts)):
        pts[i, :] = np.array([json_pts[i]["lat"], json_pts[i]["lon"], json_pts[i]["alt"]])
        pts[i, :] = wgs2utm_proj4(pts[i, :])
    return pts


# json转为dict
def json_item_to_dict(json_data):
    dict_data = {}
    for data in json_data:
        id = data["m_id"]
        json_pts = data["m_kpoints"]
        dict_data[id] = json_pts_to_np(json_pts)
    return dict_data


# 杆的距离
def pole_distance(pts_a, pts_b):
    vec_a = pts_a[1, :] - pts_a[0, :]
    vec_a = vec_a / np.linalg.norm(vec_a)
    vec_proj = vec_a * (pts_b[0, :] - pts_a[0, :]).dot(vec_a.T)
    vec_dis = (pts_b[0, :] - pts_a[0, :]) - vec_proj
    return np.linalg.norm(vec_dis)


# 牌的距离
def trafficsign_distance(pts_a, pts_b):
    vec_dis = pts_a - pts_b
    dists = np.linalg.norm(vec_dis, axis=1)
    return dists.min()

poles_tt_d = {}
trafficsigns_tt_d = {}
trafficsigns_map_d = {}
poles_map_d = {}

#匹配全站仪和地图数据
def assign_id_with_nearest_map_id(total_station_json, map_json):
    global poles_tt_d
    global trafficsigns_tt_d
    global trafficsigns_map_d
    global poles_map_d

    # poles_tt = {}
    # poles_map = {}
    #转为全站仪dict数据
    poles_tt = json_item_to_dict(total_station_json["m_pole_list"])
    #转为地图dict数据
    poles_map = json_item_to_dict(map_json["m_pole_list"])
    # get map id by distance
    tt_id_map_id_dict = {}
    for tt_id, tt_pts in poles_tt.items():
        for map_id, map_pts in poles_map.items():
            #地图和全站仪的杆的距离
            dist = pole_distance(tt_pts, map_pts)
            if (dist < 0.8):
                #如果距离小于0.8，则这俩是一个杆
                tt_id_map_id_dict[tt_id] = map_id
                continue
    # set id
    for item in total_station_json["m_pole_list"]:
        original_id = item["m_id"]#将全站仪数据中的m_id都取出，并赋值到original_id
        if (original_id in tt_id_map_id_dict):#如果全站仪和地图融合的dict中有全站仪的id
            item["m_id"] = tt_id_map_id_dict[original_id]#将全站仪json数据id对应的内容替换为融合的dict中相同id对应的内容

    # trafficsigns_tt = {}
    # trafficsigns_map = {}
    trafficsigns_tt = json_item_to_dict(total_station_json["m_traffic_sign_list"])
    trafficsigns_map = json_item_to_dict(map_json["m_traffic_sign_list"])
    # get map id by distance
    tt_id_map_id_dict = {}
    for tt_id, tt_pts in trafficsigns_tt.items():
        for map_id, map_pts in trafficsigns_map.items():
            if (tt_pts.shape[0] != map_pts.shape[0]):
                continue
            dist = trafficsign_distance(tt_pts, map_pts)
            if (dist < 0.8):
                tt_id_map_id_dict[tt_id] = map_id
                continue
    # set id
    for item in total_station_json["m_traffic_sign_list"]:
        original_id = item["m_id"]
        if (original_id in tt_id_map_id_dict):
            item["m_id"] = tt_id_map_id_dict[original_id]

    # visualize debug   画图
    #  total station data
    poles_tt_d = poles_tt
    trafficsigns_tt_d = trafficsigns_tt
    for id, pts in poles_tt_d.items():
        poles_tt_d[id] = pts.tolist()
        plt.plot([pts[0, 0]], [pts[0, 1]], "*r")
        plt.annotate("%s" % id, [pts[0, 0], pts[0, 1]])

    for id, pts in trafficsigns_tt.items():
        trafficsigns_tt_d[id] = pts.tolist()
        plt.plot(pts[0:2, 0], pts[0:2, 1], "-or")
        plt.annotate("%s" % id, [pts[0, 0], pts[0, 1]])

    #  map data
    poles_map_d = poles_map
    trafficsigns_map_d = trafficsigns_map
    x_range = [274500, 276250]#x轴的坐标区间值
    y_range = [3479000, 3480200]#y轴的坐标区间值
    for id, pts in poles_map_d.items():#对地图杆进行遍历
        poles_map_d[id] = pts.tolist()
        if (pts[0, 0] < x_range[0] or pts[0, 0] > x_range[1] or
                pts[0, 1] < y_range[0] or pts[0, 1] > y_range[1]):
            continue
        plt.plot([pts[0, 0]], [pts[0, 1]], "*g")#杆是绿色星形
        plt.annotate("%s" % id, [pts[0, 0], pts[0, 1]])
    print("poles_map.items():",poles_map_d)

    for id, pts in trafficsigns_map_d.items():
        trafficsigns_map_d[id] = pts.tolist()
        if (pts[0, 0] < x_range[0] or pts[0, 0] > x_range[1] or
                pts[0, 1] < y_range[0] or pts[0, 1] > y_range[1]):
            continue
        plt.plot(pts[0:2, 0], pts[0:2, 1], "-og")#牌是绿色实心圆
        plt.annotate("%s" % id, [pts[0, 0], pts[0, 1]])
    plt.title("total_station_pole *r\nmap_pole *g\ntotal_station_trafficsign -or\nmap_trafficsign -og")
    plt.show()

    return total_station_json

def write_file(text,name):
    # print(111)
    # print(text)
    # print(name)
    # print(222)
    if os.path.exists('/media/haoran/8E1C103E1C1023AD/test/'+name+'.txt'):
        saveFile = open('/media/haoran/8E1C103E1C1023AD/test/'+name+'.txt','w')
        saveFile.writelines(text)
        saveFile.close() #操作完文件后一定要关闭，释放内存资源
    else:
        os.mknod("/media/haoran/8E1C103E1C1023AD/test/"+name+".txt")
        saveFile = open('/media/haoran/8E1C103E1C1023AD/test/'+name+'.txt', 'w')
        saveFile.writelines(text)
        saveFile.close()  # 操作完文件后一定要关闭，释放内存资源

def write_excel_xls(path,sheet_name,value):
    index = len(value)
    workbook = xlwt.Workbook()
    sheet = workbook.add_sheet(sheet_name)
    for i in range(0, index):
        for j in range(0, len(value[i])):
            sheet.write(i, j, value[i][j])
    workbook.save(path)

def write_excel_xls_append(path, value):
    index = len(value)
    workbook = xlrd.open_workbook(path)
    sheets = workbook.sheet_names()
    worksheet = workbook.sheet_by_name(sheets[0])
    rows_old = worksheet.nrows
    new_workbook = copy(workbook)
    new_worksheet = new_workbook.get_sheet(0)
    for i in range(0, index):
        for j in range(0,len(value[i])):
            new_worksheet.write(i+rows_old,j,value[i][j])
    new_workbook.save(path)
    print("xls[追加]写入数据成功")

def read_excel_xls(path):
    workbook = xlrd.open_workbook(path)
    sheets = workbook.sheet_names()
    worksheet = workbook.sheet_by_name(sheets[0])
    for i in range(0, worksheet.nrows):
        for j in range(0, worksheet.ncols):
            print(worksheet.cell_value(i,j), "\t", end="")
        print()

book_name_xls = '/media/haoran/8E1C103E1C1023AD/test/测试报告.xls'
sheet_name_xls = '验图测试结果'
value_title = [["元素", "全站仪", "地图", "匹配<1M", "精度正常-误差<30CM","精度异常-误差>30CM","多建","漏建"], ]


def main():
    total_station_data_path = "/home/haoran/桌面/sum_total_station_point.txt"
    total_station_json_path = "/media/haoran/8E1C103E1C1023AD/test/sum_total_station_point.json"
    total_station_id_matched_json_path = "/media/haoran/8E1C103E1C1023AD/test/sum_total_station_point_matched.json"
    map_path = "/media/haoran/8E1C103E1C1023AD/test/output0820_format_remove_duplicate.json"

    # load data
    txt_data = np.loadtxt(total_station_data_path, dtype=str)

    # convert to map json
    json_data = txt_to_json(txt_data)

    # save to json file
    with open(total_station_json_path, "w") as fout:
        json.dump(json_data, fout, indent=2)

    # load map
    with open(map_path, "r") as fin:
        real_json_data = json.load(fin)
    #json_data为全站仪数据，real_json_data为地图数据
    json_data = assign_id_with_nearest_map_id(json_data, real_json_data)
    write_file(json.dumps(json_data, indent=2), "result")
    print(type(poles_tt_d))
    print("poles_tt_d:",poles_tt_d)
    write_file(json.dumps(poles_tt_d, indent=2),"poles_tt_d")
    write_file(json.dumps(trafficsigns_tt_d, indent=2),"trafficsigns_tt_d")
    write_file(json.dumps(poles_map_d, indent=2),"poles_map_d")
    write_file(json.dumps(trafficsigns_map_d, indent=2),"trafficsigns_map_d")


    with open(total_station_json_path, "w") as fout:
        json.dump(json_data, fout, indent=2)

    # save to json file with map id
    with open(total_station_id_matched_json_path, "w") as fout:
        json.dump(json_data, fout, indent=2)
    print("sheet_name_xls:",sheet_name_xls)
    value1 = [["交通牌", len(trafficsigns_tt_d), len(trafficsigns_map_d), "", ""],
              ["路灯杆", len(poles_tt_d), len(poles_map_d), "", ""],
              ["车道线端点", "", "", "", ""],
              ["车道线", "D", "C", "B", "A"],
              ["总计", "D", "C", "B", "A"]]
    write_excel_xls(book_name_xls, sheet_name_xls, value_title)
    write_excel_xls_append(book_name_xls, value1)
    # write_excel_xls_append(book_name_xls, value2)
    read_excel_xls(book_name_xls)

if __name__ == "__main__":
    main()
