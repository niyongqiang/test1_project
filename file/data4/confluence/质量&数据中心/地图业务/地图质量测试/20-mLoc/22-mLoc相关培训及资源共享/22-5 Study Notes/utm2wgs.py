#! /usr/bin/python3

import numpy as np
import sys
from pyproj import Proj
from pyproj import transform


def utm2wgs(y, x):
    utm = Proj(proj='utm', zone=51, datum='WGS84')
    ll = Proj(proj='latlong', datum='WGS84')
    (lon, lat) = transform(utm, ll, x, y)
    return lon, lat


def wgs2utm_proj4(lat, lon):
    ll = Proj(proj='latlong', datum='WGS84')
    utm = Proj(proj='utm', zone=51, datum='WGS84')
    (x, y) = transform(ll, utm, lon, lat)
    return x, y


if __name__ == '__main__':
    if len(sys.argv) < 1:
        print('usage: python ', sys.argv[0], ' filepath')
        exit(-1)

    file_path = sys.argv[1].strip()
    f_in = open(file_path, 'r')
    f_out = open(file_path.split('.')[0] + '_output.txt', 'w')
    print(f_in)
    one_line = f_in.readline().strip('\r\n').strip()
    print("one_line:", one_line)
    while one_line:
        print("one_line:", one_line)

        one_pose = one_line.split()

        print(one_pose)
        y, x = wgs2utm_proj4(float(one_pose[1]), float(one_pose[2]))
        f_out.write("%s %.8f %.8f %s\n" % (one_pose[0], x, y, one_pose[3]))

        one_line = f_in.readline().strip('\r\n').strip()
