# -- coding: utf-8 --
from __future__ import print_function
import numpy as np
import os
import sys
from matplotlib import pyplot as plt
from pyproj import Proj
from pyproj import transform

ZERO_LIKE = 5.5e-3
GPS_SEC_IDX = 1
GPS_TYPE_IDX = 5
LLA_IDX = 2
VEL_IDX = 6
ATT_IDX = 9


def wgs2utm_proj4(lla):
    ll = Proj(proj='latlong', datum='WGS84')
    utm = Proj(proj='utm', zone=50, datum='WGS84')
    (x, y) = transform(ll, utm, lla[1], lla[0])
    return np.array([x, y, lla[2]])


def utm2wgs_proj4(xyz):
    ll = Proj(proj='latlong', datum='WGS84')
    utm = Proj(proj='utm', zone=50, datum='WGS84')
    (lon, lat) = transform(utm, ll, xyz[0], xyz[1])
    return np.array([lat, lon, xyz[2]])


def llas2xyzs(llas):
    xyzs = np.empty_like(llas)
    for i in range(llas.shape[0]):
        xyzs[i] = wgs2utm_proj4(llas[i])
    return xyzs


def calcul_clrnh_rmh(llas):
    f = (1.0 / 298.257)
    e2 = 2.0 * f - f * f
    re = 6378137.0
    sls = np.sin(llas[:, 0])
    cls = np.cos(llas[:, 0])
    tls = sls / cls
    sl2s = sls**2
    sl4s = sl2s**2
    sqs = 1 - e2 * sl2s
    sq2s = sqs**0.5
    rmhs = re * (1.0 - e2) / sqs / sq2s + llas[:, 2]
    rnhs = re / sq2s + llas[:, 2]
    clrnhs = cls * rnhs
    return clrnhs, rmhs


def wgs_diffs(llas_a, llas_b):
    diffs = np.empty_like(llas_a)
    rns = np.zeros(llas_a.shape[0])
    rms = np.zeros(llas_a.shape[0])
    clrnhs, rmhs = calcul_clrnh_rmh(llas_a)

    diffs = llas_a - llas_b
    diffs[:, 0] *= rmhs
    diffs[:, 1] *= clrnhs
    diffs[:, [0, 1]] = diffs[:, [1, 0]]
    return diffs


def heading_trans(headings):
    for i in range(headings.shape[0]):
        if (headings[i] > 180.0):
            headings[i] -= 360.0
        elif (headings[i] < -180.0):
            headings[i] += 360.0
    return headings


def euler2mat(euler):
    x = euler[0] * np.pi / 180.0
    y = euler[1] * np.pi / 180.0
    z = euler[2] * np.pi / 180.0
    z = -z
    mat_z = np.array([[np.cos(z), -np.sin(z), 0.0],
                      [np.sin(z), np.cos(z), 0.0], [0.0, 0.0, 1.0]])
    mat_y = np.array([[np.cos(y), 0.0, np.sin(y)], [0.0, 1.0, 0.0],
                      [-np.sin(y), 0.0, np.cos(y)]])
    mat_x = np.array([[1.0, 0.0, 0.0], [0.0, np.cos(x), -np.sin(x)],
                      [0.0, np.sin(x), np.cos(x)]])
    return mat_z.dot(mat_x).dot(mat_y)


def decompose(gt_att, pose_diff):
    pose_diff_decomposed = np.empty_like(pose_diff)
    for i in range(gt_att.shape[0]):
        r_wi = euler2mat(gt_att[i])
        pose_diff_decomposed[i] = r_wi.T.dot(pose_diff[i])
    return pose_diff_decomposed


def time_align(data1, data2):
    i = 0
    j = 0
    k = 0
    data1_aligned = np.empty_like(data1)
    data2_aligned = np.empty_like(data2)
    while (i < data1.shape[0] and j < data2.shape[0]):
        t1 = data1[i][GPS_SEC_IDX]
        t2 = data2[j][GPS_SEC_IDX]
        if (abs(t1 - t2) < ZERO_LIKE):
            data1_aligned[k] = data1[i]
            data2_aligned[k] = data2[j]
            i += 1
            j += 1
            k += 1
        elif (t1 < t2):
            i += 1
        else:
            j += 1
    data1_aligned = data1_aligned[:k, :]
    data2_aligned = data2_aligned[:k, :]

    #  <5ms部分位置对齐
    del_t = data1_aligned[:, GPS_SEC_IDX] - data2_aligned[:, GPS_SEC_IDX]
    clrnhs, rmhs = calcul_clrnh_rmh(
        data2_aligned[:, LLA_IDX:LLA_IDX + 3] / 180.0 * np.pi)
    data2_aligned[:, LLA_IDX] += data2_aligned[:,
                                               VEL_IDX + 1] / rmhs * del_t * 180.0 / np.pi
    data2_aligned[:, LLA_IDX + 1] += data2_aligned[:,
                                                   VEL_IDX] / clrnhs * del_t * 180.0 / np.pi
    data2_aligned[:, LLA_IDX + 2] += data2_aligned[:, VEL_IDX + 2] * del_t
    return data1_aligned, data2_aligned


def eval_mean_std(data_type, data):
    rmse = np.sqrt((data ** 2).mean())
    # rmse_test = np.sqrt((np.std(data))**2 + (data.mean())**2)
    return "%s %d %.5f %.5f %.5f %0.5f %0.5f\n" % \
        (data_type, data.shape[0], data.mean(), data.max(),
         data.min(), np.std(data), rmse)


def evaluate(gt, re):
    pose_diff = wgs_diffs(re[:, LLA_IDX:LLA_IDX + 3],
                          gt[:, LLA_IDX:LLA_IDX + 3])
    vel_diff = re[:, VEL_IDX:VEL_IDX + 3] - gt[:, VEL_IDX:VEL_IDX + 3]
    att_diff = re[:, ATT_IDX:ATT_IDX + 3] - gt[:, ATT_IDX:ATT_IDX + 3]
    att_diff[:, 2] = -heading_trans(att_diff[:, 2])
    pose_diff_decomposed = decompose(gt[:, ATT_IDX:ATT_IDX + 3], pose_diff)
    # result = ""
    result = "type num mean max min std rmse\n"
    result += eval_mean_std("dpe", pose_diff[:, 0])
    result += eval_mean_std("dpn", pose_diff[:, 1])
    result += eval_mean_std("dpu", pose_diff[:, 2])
    # result += "\n"
    result += eval_mean_std("dpr", pose_diff_decomposed[:, 0])
    result += eval_mean_std("dpf", pose_diff_decomposed[:, 1])
    result += eval_mean_std("dpu", pose_diff_decomposed[:, 2])
    # result += "\n"
    result += eval_mean_std("dve", vel_diff[:, 0])
    result += eval_mean_std("dvn", vel_diff[:, 1])
    result += eval_mean_std("dvu", vel_diff[:, 2])
    # result += "\n"
    result += eval_mean_std("dap", att_diff[:, 0])
    result += eval_mean_std("dar", att_diff[:, 1])
    result += eval_mean_std("dah", att_diff[:, 2])

    fp = open('/home/duanrui/resXXX.txt', 'wt')
    for index in range(0, pose_diff.shape[0]-1):
        fp.write("{:>20}".format(str(pose_diff[index, 0])))
        fp.write('          ')
        fp.write("{:>20}".format(str(pose_diff[index, 1])))
        fp.write('          ')
        fp.write("{:>20}".format(str(pose_diff[index, 2])))
        fp.write('          ')
        fp.write('\n')
    fp.close()

    return result


def draw_result(gt_fixed, re_fixed, gt_unfixed, re_unfixed,
                gt_raw, draw_result_flag, save_result_flag, result_file_dir):
    pose_diff_fixed = wgs_diffs(
        re_fixed[:, LLA_IDX:LLA_IDX + 3], gt_fixed[:, LLA_IDX:LLA_IDX + 3])
    vel_diff_fixed = re_fixed[:, VEL_IDX:VEL_IDX +
                              3] - gt_fixed[:, VEL_IDX:VEL_IDX + 3]
    att_diff_fixed = re_fixed[:, ATT_IDX:ATT_IDX +
                              3] - gt_fixed[:, ATT_IDX:ATT_IDX + 3]
    att_diff_fixed[:, 2] = -heading_trans(att_diff_fixed[:, 2])
    pose_diff_decomposed_fixed = decompose(
        gt_fixed[:, ATT_IDX:ATT_IDX + 3], pose_diff_fixed)

    pose_diff_unfixed = wgs_diffs(
        re_unfixed[:, LLA_IDX:LLA_IDX + 3], gt_unfixed[:, LLA_IDX:LLA_IDX + 3])
    vel_diff_unfixed = re_unfixed[:, VEL_IDX:VEL_IDX +
                                  3] - gt_unfixed[:, VEL_IDX:VEL_IDX + 3]
    att_diff_unfixed = re_unfixed[:, ATT_IDX:ATT_IDX +
                                  3] - gt_unfixed[:, ATT_IDX:ATT_IDX + 3]
    att_diff_unfixed[:, 2] = -heading_trans(att_diff_unfixed[:, 2])
    pose_diff_decomposed_unfixed = decompose(
        gt_unfixed[:, ATT_IDX:ATT_IDX + 3], pose_diff_unfixed)

    fig1 = plt.figure()
    fig1.suptitle('enu position error')
    ylabels = ["e", "n", "u"]
    for i in range(3):
        ax = fig1.add_subplot(311 + i)
        l1, = ax.plot(gt_unfixed[:, GPS_SEC_IDX], pose_diff_unfixed[:,
                                                                    i], linestyle='--', marker='x', color='r')
        l2, = ax.plot(gt_fixed[:, GPS_SEC_IDX], pose_diff_fixed[:,
                                                                i], linestyle='--', marker='.', color='g')
        plt.legend(handles=[l1, l2], labels=["unfixed", "fixed"], loc='best')
        plt.ylabel(ylabels[i])
        plt.grid()

    fig2 = plt.figure()
    fig2.suptitle('rfu position error')
    ylabels = ["r", "f", "u"]
    for i in range(3):
        ax = fig2.add_subplot(311 + i)
        l1, = ax.plot(gt_unfixed[:, GPS_SEC_IDX], pose_diff_decomposed_unfixed[:,
                                                                               i], linestyle='--', marker='x', color='r')
        l2, = ax.plot(gt_fixed[:, GPS_SEC_IDX], pose_diff_decomposed_fixed[:,
                                                                           i], linestyle='--', marker='.', color='g')
        plt.legend(handles=[l1, l2], labels=["unfixed", "fixed"], loc='best')
        plt.ylabel(ylabels[i])
        plt.grid()

    fig3 = plt.figure()
    fig3.suptitle('velicity error')
    ylabels = ["e", "n", "u"]
    for i in range(3):
        ax = fig3.add_subplot(311 + i)
        l1, = ax.plot(gt_unfixed[:, GPS_SEC_IDX], vel_diff_unfixed[:,
                                                                   i], linestyle='--', marker='x', color='r')
        l2, = ax.plot(gt_fixed[:, GPS_SEC_IDX], vel_diff_fixed[:,
                                                               i], linestyle='--', marker='.', color='g')
        plt.legend(handles=[l1, l2], labels=["unfixed", "fixed"], loc='best')
        plt.ylabel(ylabels[i])
        plt.grid()

    fig4 = plt.figure()
    fig4.suptitle('att error')
    ylabels = ["pitch", "roll", "heading"]
    for i in range(3):
        ax = fig4.add_subplot(311 + i)
        l1, = ax.plot(gt_unfixed[:, GPS_SEC_IDX], att_diff_unfixed[:,
                                                                   i], linestyle='--', marker='x', color='r')
        l2, = ax.plot(gt_fixed[:, GPS_SEC_IDX], att_diff_fixed[:,
                                                               i], linestyle='--', marker='.', color='g')
        plt.legend(handles=[l1, l2], labels=["unfixed", "fixed"], loc='best')
        plt.ylabel(ylabels[i])
        plt.grid()

    fig5 = plt.figure()
    fig4.suptitle('traj')
    ax = fig5.add_subplot(111)
    l1, = ax.plot(re_unfixed[:, LLA_IDX + 1],
                  re_unfixed[:, LLA_IDX], 'r.', marker='x')
    l3, = ax.plot(re_fixed[:, LLA_IDX + 1],
                  re_fixed[:, LLA_IDX], 'g.', marker='.')
    l2, = ax.plot(gt_raw[:, LLA_IDX + 1],
                  gt_raw[:, LLA_IDX], "-", color='black')
    plt.legend(handles=[l1, l2, l3], labels=[
               "regio_unfixed", "reference", "regio_fixed"], loc='best')

    if(save_result_flag):
        fig1.savefig("1enu.png", dpi=100)
        fig2.savefig("2rfu.png", dpi=100)
        fig3.savefig("3vel.png", dpi=100)
        fig4.savefig("4att.png", dpi=100)
        fig5.savefig("5traj.png", dpi=100)

    if(draw_result_flag):
        plt.show()


def main():
    if (len(sys.argv) < 6):
        print("usage: python evaluate_result.py /path/gt /path/result /start_t /end_t /show/flag /save/flag /save/path")
    st = 0
    et = 9999999999
    if (len(sys.argv) > 4):
        st = float(sys.argv[3])
        et = float(sys.argv[4])
    show_result_flag = 1
    if (len(sys.argv) > 5):
        show_result_flag = int(sys.argv[5])
    save_result_flag = 0
    save_result_path = "./"
    if (len(sys.argv) > 6):
        save_result_flag = int(1)
        save_result_path = str(sys.argv[6])
        if not os.path.exists(save_result_path):
            os.makedirs(save_result_path)

    gt_raw = np.loadtxt(sys.argv[1])
    result_raw = np.loadtxt(sys.argv[2])

    gt_raw = gt_raw[gt_raw[:, GPS_SEC_IDX] >= st, :]
    gt_raw = gt_raw[gt_raw[:, GPS_SEC_IDX] <= et, :]

    gt_aligned, result_aligned = time_align(gt_raw, result_raw)

    gt_aligned[:, LLA_IDX:LLA_IDX + 2] *= np.pi / 180.0
    result_aligned[:, LLA_IDX:LLA_IDX + 2] *= np.pi / 180.0
    gt_raw[:, LLA_IDX:LLA_IDX + 2] *= np.pi / 180.0

    result_available = result_aligned[result_aligned[:, GPS_TYPE_IDX] > 0, :]
    gt_aligned, result_aligned_available = time_align(gt_raw, result_available)

    result_aligned_unfix = result_aligned_available[result_aligned_available[:,
                                                                             GPS_TYPE_IDX] != 4, :]
    result_aligned_fix = result_aligned_available[result_aligned_available[:,
                                                                           GPS_TYPE_IDX] == 4, :]

    gt_aligned_unfix = gt_aligned[result_aligned_available[:,
                                                           GPS_TYPE_IDX] != 4, :]
    gt_aligned_fix = gt_aligned[result_aligned_available[:,
                                                         GPS_TYPE_IDX] == 4, :]

    result_save = "%s\n%s\n%d %d\n" % (sys.argv[1], sys.argv[2], st, et)

    result_str = "all:\n"
    result_str += evaluate(gt_aligned, result_aligned_available)
    result_save += result_str
    print(result_str)

    if(result_aligned_fix.size != 0):
        result_str_fix = "fixed:\n"
        result_str_fix += evaluate(gt_aligned_fix, result_aligned_fix)
        result_save += result_str_fix
        print(result_str_fix)

    if(result_aligned_unfix.size != 0):
        result_str_unfix = "unfixed:\n"
        result_str_unfix += evaluate(gt_aligned_unfix, result_aligned_unfix)
        result_save += result_str_unfix
        print(result_str_unfix)

    if(save_result_flag):
        with open(save_result_path + "/results.txt", "w") as f_out:
            f_out.write(result_save)

    draw_result(gt_aligned_fix, result_aligned_fix, gt_aligned_unfix, result_aligned_unfix,
                gt_raw, show_result_flag, save_result_flag, save_result_path)


if __name__ == "__main__":
    main()
