# coding=utf-8
import datetime
import threading
import time
import os

# def func_task():
#     print("执行任务中...")


# def func_timer():

#     func_task()
#     global timer  # 定义全局变量
#     # 定时器构造函数主要有2个参数，第一个参数为时间，第二个参数为函数名
#     timer = threading.Timer(10, func_timer)   # 10秒调用一次函数

#     print("线程名称={},\n正在执行的线程列表:{},\n正在执行的线程数量={},\n当前激活线程={}\n".format(
#         timer.getName(), threading.enumerate(), threading.active_count(), timer.isAlive)
#     )

#     timer.start()  # 启用定时器


# timer = threading.Timer(1, func_timer)
# timer.start()
# print('定时器启动成功-----')

while True:
    time_now = time.strftime("%H:%M:%S", time.localtime())  # 刷新
    if time_now == "03:00:00":  # 此处设置每天定时的时间

        # 此处3行替换为需要执行的动作
        print("hello")
        subject = time.strftime("%Y-%m-%d %H:%M:%S",
                                time.localtime()) + " 定时发送测试"
        print(subject)

        time.sleep(2)  # 因为以秒定时，所以暂停2秒，使之不会在1秒内执行多次

        os.system('sudo shutdown -r now')
