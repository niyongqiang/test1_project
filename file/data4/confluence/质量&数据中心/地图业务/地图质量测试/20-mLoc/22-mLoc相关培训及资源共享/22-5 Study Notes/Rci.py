import sys
import os
import codecs
import numpy.matlib
import numpy as np
import math
import pandas as pd
import shutil
import json
from MyEncoder import MyEncoder


data_path = sys.argv[1]
data_list = os.listdir(data_path)

# 获取device.json文件路径，并且创建一个新的device1.json
device_path = ''
for i in range(0, len(data_list)):
    data_full_path = os.path.join(data_path, data_list[i])
    if os.path.exists(data_full_path) and data_full_path.endswith("device.json"):
        device_path = data_full_path
print("device_path:%s" % (device_path))
device1_path = os.path.join(data_path, "device1.json")
shutil.copyfile(device_path, (device1_path))


# 获取giohp/mapecu.frontend.ref.emsfr.txt文件路径
Filelist = []
result_path = ''
result_path_gio = ''
for home, dirs, files in os.walk(data_path):
    for filename in files:
        emsfr_path = os.path.join(home, filename)
        if emsfr_path.endswith("giohp/mapecu.frontend.ref.emsfr.txt"):
            result_path = emsfr_path
        elif emsfr_path.endswith("gio/mapecu.frontend.ref.emsfr.txt"):
            result_path_gio = emsfr_path
print("result_path:%s" % (result_path))
print("result_path_gio:%s" % (result_path_gio))

list_r = []
list_c = []
list_i = []

list_r_gio = []
list_c_gio = []
list_i_gio = []
##########取giohp的rvi值#########
# 解状态大于0时间段[x],取所有解状态大于0的行数index_arr
data = np.loadtxt(result_path)
status = data[:, 5]
r = data[:, 20]
v = data[:, 21]
i = data[:, 22]
index_arr = np.argwhere(status > 0)
print(index_arr)
# 然后根据index_arr取所有解状态大于0的rvi的值
list_r = r[index_arr]
list_v = v[index_arr]
list_i = i[index_arr]
print(list_r)
##########取gio的rvi值#########
data_gio = np.loadtxt(result_path_gio)
r_gio = data_gio[:, 20]
v_gio = data_gio[:, 21]
i_gio = data_gio[:, 22]
list_r_gio = r_gio[index_arr]
list_v_gio = v_gio[index_arr]
list_i_gio = i_gio[index_arr]

# avg:giohp的rvi均值
r_avg = np.mean(list_r)
v_avg = np.mean(list_v)
i_avg = np.mean(list_i)
# avg:gio的rvi均值
r_avg_gio = np.mean(list_r_gio)
v_avg_gio = np.mean(list_v_gio)
i_avg_gio = np.mean(list_i_gio)

print("r_avg:%d" % (r_avg))
print("v_avg:%d" % (v_avg))
print("i_avg:%d" % (i_avg))
print(type(r_avg_gio))


print("r_avg_gio:%d" % r_avg_gio)
print("v_avg_gio:%d" % v_avg_gio)
print("i_avg_gio:%d" % i_avg_gio)

r_avg = r_avg.astype(np.int16)
r_avg = int(r_avg)
v_avg = v_avg.astype(np.int16)
v_avg = int(v_avg)
i_avg = i_avg.astype(np.int16)
i_avg = int(i_avg)
r_avg_gio = r_avg_gio.astype(np.int16)
r_avg_gio = int(r_avg_gio)
v_avg_gio = v_avg_gio.astype(np.int16)
v_avg_gio = int(v_avg_gio)
i_avg_gio = i_avg_gio.astype(np.int16)
i_avg_gio = int(i_avg_gio)
rvi_giohp = []
rvi_gio = []
# 获取解状态大于0的rvi均值 rvi1 = rvi_gio rvi_2 = rvi_giohp
rvi_giohp = np.array([r_avg, v_avg, i_avg])
rvi_gio = np.array([r_avg_gio, v_avg_gio, i_avg_gio])
print(rvi_giohp)
print(rvi_gio)

# r_avg1 = math.radians(r_avg)
# v_avg1 = math.radians(v_avg)
# i_avg1 = math.radians(i_avg)
#
# r_avg_gio1 = math.radians(r_avg_gio)
# v_avg_gio1 = math.radians(v_avg_gio)
# i_avg_gio1 = math.radians(i_avg_gio)


def EulerToMatrix(euler):

    si = np.sin(euler[0])
    ci = np.cos(euler[0])
    sj = np.sin(euler[1])
    cj = np.cos(euler[1])
    sk = np.sin(euler[2])
    ck = np.cos(euler[2])
    # print(si)
    # print(ci)
    # print(sj)
    # print(cj)
    # print(sk)
    # print(ck)

    mat = np.zeros((3, 3))
    mat[0, 0] = cj * ck - si * sj * sk
    mat[0, 1] = -ci * sk
    mat[0, 2] = sj * ck + si * cj * sk
    mat[1, 0] = cj * sk + si * sj * ck
    mat[1, 1] = ci * ck
    mat[1, 2] = sj * sk - si * cj * ck
    mat[2, 0] = -ci * sj
    mat[2, 1] = si
    mat[2, 2] = ci * cj

    return mat


# data = pd.DataFrame(pd.read_json(device_path),columns = ["extrinsic"],index = ["R_cam_imu"])
# print("11111")
# print(data)
# data1 = np.array(data)
# data1 = data1.tolist()
# print("1222222")
# print(data1)
with open(device_path, 'r') as f:
    data = json.load(f)
    data1 = data["extrinsic"]["R_cam_imu"]
    print(data1)
# 得到最终转化为弧度的delta_rvi
    delta_rvi_d = rvi_gio-rvi_giohp
    r = math.radians(delta_rvi_d[0])
    v = math.radians(delta_rvi_d[1])
    i = math.radians(delta_rvi_d[2])
    print(delta_rvi_d)
    delta_rvi = np.array([r, v, i])
    print(delta_rvi)

    result_EulerToMatrix = EulerToMatrix(delta_rvi)
    print(result_EulerToMatrix)

    # r_ci_ = result_EulerToMatrix*data1,得到最终新的r_ci_
    result_EulerToMatrix = np.squeeze(np.asarray(result_EulerToMatrix))
    data1 = np.squeeze(np.asarray(data1))
    data1 = np.array(data1).reshape(3, 3)
    r_ci_ = np.dot(result_EulerToMatrix, data1)
    # r_ci_ = []
    print(r_ci_)


# new_data = pd.DataFrame(pd.read_json(device1_path))
# new = pd.DataFrame(pd.read_json(device1_path),columns = ["extrinsic"],index = ["R_cam_imu"])
# print(new)
# new_data.loc['R_cam_imu','extrinsic'] = '1'
# print(new_data)
    rci = np.array(r_ci_).reshape(1, 9)
    data["extrinsic"]["R_cam_imu"] = rci

    print(data)
    # new_json_dict = data.to_dict()
    with open(device1_path, 'w') as fout:
        json.dump(data, fout, ensure_ascii=False, cls=MyEncoder, indent=2)
#
#
# new_json_dict = new_data.to_dict()
# with open("/home/haoran/桌面/aaa.json", 'w') as fout:
#     json.dump(new_json_dict, fout,indent=4)
