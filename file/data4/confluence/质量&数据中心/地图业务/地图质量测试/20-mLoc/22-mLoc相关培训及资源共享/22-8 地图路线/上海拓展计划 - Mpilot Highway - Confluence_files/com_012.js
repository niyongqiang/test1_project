WRMCB=function(e){var c=console;if(c&&c.log&&c.error){c.log('Error running batched script.');c.error(e);}}
;
try {
/* module-key = 'com.atlassian.confluence.plugins.confluence-previews:feature-discovery-plugin', location = 'js/component/feature-discovery/discovery-dialog.js' */
define("cp/component/feature-discovery/discovery-dialog",["jquery","underscore","skate","core/template-store-singleton","aui/inline-dialog2"],function(f,c,a,d){var e=function(h,g){return function(){h.trigger(g,arguments)}};var b=function(g){this.$anchor=f(g.anchor);this.$appendTo=f(g.appendTo);this.key=g.key;this.text=g.text;this.dialog=null;this.$anchor.attr({"aria-controls":"cp-feature-discovery","data-aui-trigger":true});var i=f(d.get("FeatureDiscovery.featureDiscovery")({text:this.text}));i.find(".cp-feature-discovery-confirm").click(function(j){this.dismiss(true);j.preventDefault()}.bind(this));var h=i[0];this.$appendTo.append(h);a.init(h);h.show();this.dialog=h;c.extend(this,Backbone.Events);this.dialog.addEventListener("aui-layer-hide",e(this,"hide"));this.dialog.addEventListener("aui-layer-show",e(this,"show"));return this};b.prototype.is=function(g){return this.key===g};b.prototype.on=function(h,i){var g=h;if(h==="hide"){g="aui-layer-hide"}else{if(h==="show"){g="aui-layer-show"}}this.dialog.addEventListener(g,i)};b.prototype.dismiss=function(g){this.dialog.hide();this.dialog.remove();g&&this.trigger("user-dismissed-dialog",this.key)};return b});
}catch(e){WRMCB(e)};
;
try {
/* module-key = 'com.atlassian.confluence.plugins.confluence-previews:feature-discovery-plugin', location = '/templates/feature-discovery.soy' */
// This file was automatically generated from feature-discovery.soy.
// Please don't edit this file by hand.

/**
 * @fileoverview Templates in namespace FileViewer.Templates.FeatureDiscovery.
 */

if (typeof FileViewer == 'undefined') { var FileViewer = {}; }
if (typeof FileViewer.Templates == 'undefined') { FileViewer.Templates = {}; }
if (typeof FileViewer.Templates.FeatureDiscovery == 'undefined') { FileViewer.Templates.FeatureDiscovery = {}; }


FileViewer.Templates.FeatureDiscovery.featureDiscovery = function(opt_data, opt_ignored) {
  return '<aui-inline-dialog2 id="cp-feature-discovery" class="aui-layer aui-inline-dialog" data-aui-alignment="bottom center" data-aui-responds-to="toggle" data-aui-persistent="true" data-aui-focus="false"><div class="aui-inline-dialog-contents"><p>' + soy.$$escapeHtml(opt_data.text) + '</p><button class="aui-button aui-button-link cp-feature-discovery-confirm">' + soy.$$escapeHtml('\u6e05\u9664') + '</button></div></aui-inline-dialog2>';
};
if (goog.DEBUG) {
  FileViewer.Templates.FeatureDiscovery.featureDiscovery.soyTemplateName = 'FileViewer.Templates.FeatureDiscovery.featureDiscovery';
}

}catch(e){WRMCB(e)};
;
try {
/* module-key = 'com.atlassian.confluence.plugins.confluence-previews:feature-discovery-plugin', location = 'js/component/feature-discovery/feature-discovery-plugin.js' */
define("cp/component/feature-discovery/feature-discovery-plugin",["cp/component/feature-discovery/discovery-dialog","underscore","ajs","confluence/legacy"],function(e,h,f,g){var d=null;var c=g.FeatureDiscovery.forPlugin("com.atlassian.confluence.plugins.confluence-previews");var a=function(j){if(d===null){return}if(!j.key||j.key&&d.is(j.key)){d.dismiss(j.persist)}};var b=function(k){var j=k.key;var l=$(k.anchor);if(l.length===0||!l.is(":visible")||l.closest("body").length===0||!c.shouldShow(j)){return}c.addDiscoveryView(k.key);a({persist:false});d=new e(k);d.on("user-dismissed-dialog",function(m){c.markDiscovered(m)});return d};var i=function(l){var n=f.Meta.get("remote-user");var k="view-annotations";var o="add-annotations";var m=[k,o];var j=n&&h(m).without(c.listDiscovered()).length!==0;if(!j){return}l.on("fv.showFile",function(t){a({persist:false});var v=t.get("annotations");var s=l.supports(t.get("type"));var u=t.get("isRemoteLink");if(!v||!s||u){return}var r=l.getView().fileSidebarView;var q=l.getView().fileControlsView;var p=q.getLayerForName("annotationButton");var w=l.getView().fileContentView.getLayerForName("content")._viewer;v.on("sync",function(){var x=v.getCount();var y=r.isPanelInitialized("annotations");if(x&&!y){b({key:k,anchor:p.$("a"),text:"\u8bf6\u5440\uff0c\u6ce8\u91ca\uff01\u67e5\u770b\u4ec0\u4e48\u4eba\u6b63\u5728\u8bc4\u8bba\u8fd9\u4e2a\u6587\u4ef6\u3002",appendTo:l.getView().$el})}else{var A=w.$("#cp-file-control-annotate");w.showControls();var z=b({key:o,anchor:A,text:"\u8fd9\u4e2a\u5c0f\u5f15\u811a\u60f3\u548c\u4f60\u5408\u4f5c\u3002\u62d6\u5b83\u5230\u4efb\u4f55\u5730\u65b9\u6765\u6dfb\u52a0\u8bc4\u8bba\u3002",appendTo:l.getView().$el});if(z){w.autoToggleControls(false);w.showControls();A.mousedown(function(){a({key:o,persist:true})});z.on("hide",function(){w.autoToggleControls(true)})}}})});l.on("fv.changeFile fv.close",function(){a({persist:false})});l.getView().fileSidebarView.on("togglePanel",function(q,p){if(q!=="annotations"||p===false){return}a({key:"view-annotations",persist:true});a({persist:false})});l.getView().fileSinkView.on("togglePanel",function(){a({persist:false})})};return i});(function(){var b=require("cp/component/feature-discovery/feature-discovery-plugin");var a=require("MediaViewer");a.registerPlugin("featureDiscovery",b)})();
}catch(e){WRMCB(e)};