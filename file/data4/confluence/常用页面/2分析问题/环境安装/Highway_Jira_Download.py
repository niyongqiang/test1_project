# coding: utf-8
from jira import JIRA
import os
import subprocess
import sys
import re
import shlex

while 1:
    bag_dir = "./"
    aws_link_matcher = re.compile(r"(aws\s.*\.bag\s\.)", re.MULTILINE)
    username = "v-niyongqiang"
    password = "Woaini56294"
    test_jira = JIRA("https://jira.momenta.works", auth=(username, password))

    if sys.version_info < (3,0):
        key_numbers = raw_input("请选择输入编号(XXX 或 XXX-XXX,以逗号分割),结束输入exit：") #python2 support
    else:
        key_numbers = input("请选择输入编号(XXX 或 XXX-XXX,以逗号分割),结束输入exit：")
    numbers = []
    if str(key_numbers) == "exit": break

    key_numbers = re.sub(r'[，]{1,100}', ',', key_numbers)  # 对中文逗号匹配1到100次
    key_numbers = re.sub(r"\s+", "", key_numbers)
    numbers = key_numbers.split(',')
    while '' in numbers:
        numbers.remove('')
    while ',' in numbers:
        numbers.remove(',')
    print(numbers)

    numbers_list = []
    for num in numbers:
        if '-' in num:
            temp = num.split('-')
            start = int(temp[0])
            end = int(temp[1])
            for val in range(start, end + 1):
                numbers_list.append(val)
        else:
            numbers_list.append(num)
    print(numbers_list)

    issues_all = []
    for i in numbers_list:
        search_str = 'project = "MHT" AND key = ''MHT-' + str(i) + ' '
        issues = test_jira.search_issues(search_str, maxResults=10000, fields='description')
        issues_all.append(issues)

    cmd_list = []
    for i in issues_all:
        for issue in i:
            result = aws_link_matcher.search(issue.fields.description)
            cmd_list.append([result.group(1), issue.key])


    for cmd, key in cmd_list:
        command = shlex.split(cmd)
        command[-1] = os.path.join(bag_dir, key + '_' + os.path.basename(command[-2]))
        subprocess.check_call(command, stdout = None, stderr = None)
